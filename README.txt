This module provides a webform component for taking payments via Stripe.

# Installation

1. Install module in the usual way, e.g. in sites/all/modules.
2. Go to https://github.com/stripe/stripe-php/releases and download a release (tested with v7.2.2). It should be
   extracted to sites/all/libraries/stripe-php.
3. Enable the module.
4. Add API keys at /admin/config/services/webform_stripe_payment
5. Add the component to a webform and configure at least the currency and amount with a fixed amount or get them from
   another webform component.
