<?php

/**
 * Implements _webform_defaults_[component]().
 */
function _webform_defaults_stripe_payment() {
  $element = array(
    'name' => 'Stripe payment',
    'form_key' => NULL,
    'mandatory' => 1,
    'value' => array(),
    'pid' => 0,
    'weight' => 0,
    'extra' => array(
      'payment_description' => 'Default payment',
      'currency_code' => 'GBP',
      'currency_code_source' => 'fixed',
      'currency_code_component' => NULL,
      'amount' => '10.00',
      'amount_source' => 'fixed',
      'amount_component' => NULL,
      'email_component' => NULL,
      'address_line1_component' => NULL,
      'address_line2_component' => NULL,
      'address_city_component' => NULL,
      'address_state_component' => NULL,
      'address_country_component' => NULL,
      'receipt_email_component' => NULL,
      'name_component' => NULL,
      'first_name_component' => NULL,
      'last_name_component' => NULL,
      'collapsible' => FALSE,
      'collapsed' => FALSE,
    ),
  );

  return $element;
}

/**
 * Implements _webform_edit_[component]().
 */
function _webform_edit_stripe_payment($component) {
  $form = array(
    'extra' => array(),
  );

  $form['extra']['payment_description'] = array(
    '#type' => 'textfield',
    '#title' => t('Payment description'),
    '#default_value' => $component['extra']['payment_description'],
    '#required' => TRUE,
  );

  $form['extra']['currency_code'] = array(
    '#type' => 'container',
    '#element_validate' => array('_webform_stripe_payment_currency_values'),
  );

  $radio_class = drupal_html_id('webform-stripe-payment-currency');
  $form['extra']['currency_code']['source'] = array(
    '#title' => t('Currency'),
    '#type' => 'radios',
    '#options' => array(
      'fixed' => t('Use a fixed currency'),
      'component' => t('Read the currency from another form element'),
    ),
    '#default_value' => $component['extra']['currency_code_source'],
    '#attributes' => array(
      'class' => array($radio_class),
    ),
  );

  // Todo use a select input with options from currency module.
  $form['extra']['currency_code']['value'] = array(
    '#title' => t('Fixed currency code'),
    '#description'   => t('Three letter currency code.'),
    '#type'          => 'textfield',
    '#default_value' => $component['extra']['currency_code'],
    '#states'        => array(
      'visible' => array(".$radio_class" => array(
        'value' => 'fixed',
      )),
    ),
  );

  // Todo use a select list containing the other webform elements.
  $form['extra']['currency_code']['component'] = array(
    '#title' => t('Currency code component'),
    '#description'   => t('Enter the ID (integer) of a component in this webform which holds the currency code.'),
    '#type'          => 'textfield',
    '#default_value' => $component['extra']['currency_code_component'],
    '#states'        => array(
      'visible' => array(".$radio_class" => array(
        'value' => 'component',
      )),
    ),
  );

  $form['extra']['amount'] = array(
    '#type' => 'container',
    '#element_validate' => array('_webform_stripe_payment_amount_values'),
  );

  $radio_class = drupal_html_id('webform-stripe-payment-amount');
  $form['extra']['amount']['source'] = array(
    '#title' => t('Amount'),
    '#type' => 'radios',
    '#options' => array(
      'fixed' => t('Use a fixed amount'),
      'component' => t('Read the amount from another form element'),
    ),
    '#default_value' => $component['extra']['amount_source'],
    '#attributes' => array(
      'class' => array($radio_class),
    ),
  );

  $form['extra']['amount']['value'] = array(
    '#title' => t('Fixed amount'),
    '#description'   => t('The amount e.g. 10.00'),
    '#type'          => 'textfield',
    '#default_value' => $component['extra']['amount'],
    '#states'        => array(
      'visible' => array(".$radio_class" => array(
        'value' => 'fixed',
      )),
    ),
  );

  $form['extra']['amount']['component'] = array(
    '#title' => t('Amount component'),
    '#description'   => t('Enter the ID (integer) of a component in this webform which holds the amount.'),
    '#type'          => 'textfield',
    '#default_value' => $component['extra']['amount_component'],
    '#states'        => array(
      'visible' => array(".$radio_class" => array(
        'value' => 'component',
      )),
    ),
  );

  $form['extra']['email_component'] = array(
    '#title' => t('Email component'),
    '#description'   => t('Enter the ID (integer) of a component in this webform which holds the customer\'s email address.'),
    '#type'          => 'textfield',
    '#default_value' => $component['extra']['email_component'],
  );

  $form['extra']['receipt_email_component'] = array(
    '#title' => t('Receipt email component'),
    '#description'   => t('Enter the ID (integer) of a component in this webform which holds the customer\'s email address. Used for sending a receipt via Stripe.'),
    '#type'          => 'textfield',
    '#default_value' => $component['extra']['receipt_email_component'],
  );

  $form['extra']['address_line1_component'] = array(
    '#title' => t('Address line 1 component'),
    '#description'   => t('Enter the ID (integer) of a component in this webform which holds the customer\'s address line 1.'),
    '#type'          => 'textfield',
    '#default_value' => $component['extra']['address_line1_component'],
  );

  $form['extra']['address_line2_component'] = array(
    '#title' => t('Address line 2 component'),
    '#description'   => t('Enter the ID (integer) of a component in this webform which holds the customer\'s address line 2.'),
    '#type'          => 'textfield',
    '#default_value' => $component['extra']['address_line2_component'],
  );

  $form['extra']['address_city_component'] = array(
    '#title' => t('Address city component'),
    '#description'   => t('Enter the ID (integer) of a component in this webform which holds the customer\'s address city.'),
    '#type'          => 'textfield',
    '#default_value' => $component['extra']['address_city_component'],
  );

  $form['extra']['address_state_component'] = array(
    '#title' => t('Address state component'),
    '#description'   => t('Enter the ID (integer) of a component in this webform which holds the customer\'s address state.'),
    '#type'          => 'textfield',
    '#default_value' => $component['extra']['address_state_component'],
  );

  $form['extra']['address_country_component'] = array(
    '#title' => t('Address country component'),
    '#description'   => t('Enter the ID (integer) of a component in this webform which holds the customer\'s address country.'),
    '#type'          => 'textfield',
    '#default_value' => $component['extra']['address_country_component'],
  );

  $form['extra']['name_component'] = array(
    '#title' => t('Name component'),
    '#description'   => t('Enter the ID (integer) of a component in this webform which holds the customer\'s name. Please specify either this or components for first and last name.'),
    '#type'          => 'textfield',
    '#default_value' => $component['extra']['name_component'],
  );

  $form['extra']['first_name_component'] = array(
    '#title' => t('First name component'),
    '#description'   => t('Enter the ID (integer) of a component in this webform which holds the customer\'s first name.'),
    '#type'          => 'textfield',
    '#default_value' => $component['extra']['first_name_component'],
  );

  $form['extra']['last_name_component'] = array(
    '#title' => t('Last name component'),
    '#description'   => t('Enter the ID (integer) of a component in this webform which holds the customer\'s last name.'),
    '#type'          => 'textfield',
    '#default_value' => $component['extra']['last_name_component'],
  );

  return $form;
}

/**
 * Element validate function for the currency component or value.
 */
function _webform_stripe_payment_currency_values($element, &$form_state, $form) {
  $parents_parents = array_splice($element['#parents'], 0, -1);
  $values = &drupal_array_get_nested_value($form_state['values'], $parents_parents);

  $values['currency_code_source'] = $values['currency_code']['source'];
  $values['currency_code_component'] = $values['currency_code']['component'];
  $values['currency_code'] = $values['currency_code']['value'];
}

/**
 * Element validate function for the amount component or value.
 */
function _webform_stripe_payment_amount_values($element, &$form_state, $form) {
  $parents_parents = array_splice($element['#parents'], 0, -1);
  $values = &drupal_array_get_nested_value($form_state['values'], $parents_parents);

  $values['amount_source'] = $values['amount']['source'];
  $values['amount_component'] = $values['amount']['component'];
  $values['amount'] = $values['amount']['value'];
}

/**
 * Implements _webform_render_[component]().
 */
function _webform_render_stripe_payment($component, $value = NULL, $filter = TRUE) {
  $defaults = _webform_defaults_stripe_payment();
  $component += $defaults;

  $node = isset($component['nid']) ? node_load($component['nid']) : NULL;

  libraries_load('stripe-php');

  $element = array(
    '#type' => 'container',
    '#theme_wrappers' => array('webform_element'),
    '#title' => $filter ? webform_filter_xss($component['name']) : $component['name'],
    '#title_display' => $component['extra']['title_display'] ? $component['extra']['title_display'] : 'before',
    '#description' => $filter ? webform_filter_descriptions($component['extra']['description'], $node) : $component['extra']['description'],
    '#weight' => isset($component['weight']) ? $component['weight'] : 0,
    '#currency_code' => $component['extra']['currency_code'],
    '#amount' => $component['extra']['amount'],
    '#payment_description' => $component['extra']['payment_description'],
    'card_element' => array(
      '#type' => 'container',
      '#attributes' => array(
        'id' => 'stripe-card',
      ),
    ),
    'stripe_errors' => array(
      '#type' => 'container',
      '#attributes' => array(
        'id' => 'stripe-errors',
        'class' => array('error'),
      ),
    ),
    'payment_method' => array(
      '#type' => 'hidden',
      '#attributes' => array(
        'id' => 'stripe-payment-method',
      ),
      '#tree' => FALSE,
    ),
    'payment_intent' => array(
      '#type' => 'hidden',
      '#attributes' => array(
        'id' => 'stripe-payment-intent',
      ),
      '#tree' => FALSE,
    ),
    'payment_complete' => array(
      '#type' => 'hidden',
      '#attributes' => array(
        'id' => 'stripe-payment-complete',
      ),
      '#tree' => FALSE,
    ),
    '#attached' => array(
      'js' => array(
        'misc/ajax.js',
        'misc/states.js',
        array(
          'type' => 'external',
          'data' => 'https://js.stripe.com/v3/',
        ),
        array(
          'type' => 'file',
          'data' => drupal_get_path('module', 'webform_stripe_payment') . '/stripe.js',
        ),
        array(
          'type' => 'setting',
          'data' => array(
            'webform_stripe_payment' => array(
              'public_key' => variable_get('webform_stripe_payment_public_key', ''),
            ),
          )
        ),
      ),
    ),
  );

  return $element;
}

/**
 * Implements _webform_display_[component]().
 */
function _webform_display_stripe_payment($component, $value, $format = 'html') {
  $fieldset = _webform_display_fieldset($component, NULL, $format);

  $fieldset['status'] = array(
    '#title' => t('Status'),
    '#theme' => 'webform_display_textfield',
    '#theme_wrappers' => $format == 'html' ? array('webform_element') : array('webform_element_text'),
    '#post_render' => array('webform_element_wrapper'),
    '#component' => $component,
    '#format' => $format,
    '#value' => isset($value[0]) ? $value[0] : '',
    '#parents' => array('submitted', $component['form_key']),
  );

  $fieldset['payment_id'] = array(
    '#title' => t('Payment ID'),
    '#theme' => 'webform_display_textfield',
    '#theme_wrappers' => $format == 'html' ? array('webform_element') : array('webform_element_text'),
    '#post_render' => array('webform_element_wrapper'),
    '#component' => $component,
    '#format' => $format,
    '#value' => isset($value[1]) ? $value[1] : '',
    '#parents' => array('submitted', $component['form_key']),
  );

  $fieldset['currency'] = array(
    '#title' => t('Currency'),
    '#theme' => 'webform_display_textfield',
    '#theme_wrappers' => $format == 'html' ? array('webform_element') : array('webform_element_text'),
    '#post_render' => array('webform_element_wrapper'),
    '#component' => $component,
    '#format' => $format,
    '#value' => isset($value[2]) ? $value[2] : '',
    '#parents' => array('submitted', $component['form_key']),
  );

  $fieldset['amount_received'] = array(
    '#title' => t('Amount received'),
    '#theme' => 'webform_display_textfield',
    '#theme_wrappers' => $format == 'html' ? array('webform_element') : array('webform_element_text'),
    '#post_render' => array('webform_element_wrapper'),
    '#component' => $component,
    '#format' => $format,
    '#value' => isset($value[3]) ? ($value[3] / 100) : '',
    '#parents' => array('submitted', $component['form_key']),
  );

  $fieldset['mode'] = array(
    '#title' => t('Mode'),
    '#theme' => 'webform_display_textfield',
    '#theme_wrappers' => $format == 'html' ? array('webform_element') : array('webform_element_text'),
    '#post_render' => array('webform_element_wrapper'),
    '#component' => $component,
    '#format' => $format,
    '#value' => isset($value[4]) ? ($value[4] ? 'LIVE' : 'TEST') : '',
    '#parents' => array('submitted', $component['form_key']),
  );

  return $fieldset;
}

/**
 * Implements _webform_table_[component]().
 */
function _webform_table_stripe_payment($component, $value) {
  $output = '';

  for ($i = 0; $i < 5; $i++) {
    if ($i != 0) {
      $output .= '/';
    }
    if (isset($value[$i])) {
      if ($i == 3) {
        $output .= $value[$i] / 100;
      }
      elseif ($i == 4) {
        $output .= $value[$i] ? 'LIVE' : 'TEST';
      }
      else {
        $output .= $value[$i];
      }
    }
  }

  return $output;
}
