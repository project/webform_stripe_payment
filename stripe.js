(function ($) {
  var stripe, card, $form, $submit, $cardElement, $displayError, $paymentMethod, $paymentIntent, $paymentComplete;

  Drupal.behaviors.stripe_payment = {
    attach: function(context, settings) {
      $cardElement = $('#stripe-card');
      if ($cardElement.length) {
        // Only load Stripe on the element once.
        if ($cardElement.hasClass('StripeElement')) {
          return;
        }

        $displayError = $('#stripe-errors');

        // Don't bother if there is no API key.
        if (typeof settings.webform_stripe_payment.public_key === 'undefined' || settings.webform_stripe_payment.public_key === '') {
          return;
        }

        stripe = Stripe(settings.webform_stripe_payment.public_key);
        card = stripe.elements().create('card');
        card.mount('#stripe-card');

        card.on('change', function(event) {
          if (event.error) {
            $displayError.html(event.error.message);
          }
          else {
            $displayError.html('');
          }
        });

        $paymentMethod = $('#stripe-payment-method');
        $paymentIntent = $('#stripe-payment-intent');
        $paymentComplete = $('#stripe-payment-complete');
      }
    }
  };

  function resetPayment() {
    // Reset the hidden values so we can try again.
    $paymentMethod.val('');
    $paymentIntent.val('');
    $paymentComplete.val(0);
  }

  $.fn.createPaymentMethod = function(billingDetails) {
    stripe.createPaymentMethod('card', card, billingDetails).then(function (result) {
      if (result.error) {
        $displayError.html(result.error.message);
      }
      else {
        $paymentMethod.val(result.paymentMethod.id);
        $submit.trigger('mousedown');
      }
    });
  };

  $.fn.handleServerResponse = function(result, arg) {
    switch (result) {
      case 'success':
        $paymentIntent.val(arg);
        $paymentComplete.val(1);
        $form.submit();
        break;

      case 'requires_action':
        stripe.handleCardAction(arg).then(function(result) {
          if (result.error) {
            resetPayment();
            $displayError.html(result.error.message);
          }
          else {
            $paymentIntent.val(result.paymentIntent.id);
            $submit.trigger('mousedown');
          }
        });
        break;

      default:
        resetPayment();
        $displayError.html(arg);
    }
  };

  var beforeSubmit = Drupal.ajax.prototype.beforeSubmit;
  Drupal.ajax.prototype.beforeSubmit = function (form_values, element, options) {
    // Save the submit button that was used and the form because they will be used in the response.
    $submit = $(this.element);
    $form = element;
    $displayError.html('');
    beforeSubmit(form_values, element, options);
  };

}(jQuery));
