<?php

/**
 * Respond to a Stripe payment being made in a webform.
 *
 * @param array $form
 *   The form array.
 * @param array $form_state
 *   The form_state at the time of form submission.
 * @param \stdClass $submission
 *   The webform submission.
 * @param \Stripe\PaymentIntent $intent
 *   The payment intent object from Stripe.
 */
function hook_webform_stripe_payment_complete(array $form, array $form_state, $submission, \Stripe\PaymentIntent $intent) {
  if ($intent->status == 'succeeded') {
    module_load_include('inc', 'webform', 'includes/webform.submissions');
    // Do something with submission...
    webform_submission_update($form['#node'], $submission);
  }
}
