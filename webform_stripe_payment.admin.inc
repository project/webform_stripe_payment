<?php

/**
 * Form for the Webform Stripe Payment admin page.
 */
function webform_stripe_payment_admin_form($form, &$form_state) {
  $form['instructions'] = array(
    '#type' => 'item',
    '#description' => t('You can find the API keys in your <a href="@url">Stripe account.</a>', array(
      '@url' => 'https://dashboard.stripe.com/',
    )),
  );

  $form['webform_stripe_payment_public_key'] = array(
    '#title' => t('Public key'),
    '#type' => 'textfield',
    '#default_value' => variable_get('webform_stripe_payment_public_key'),
    '#required' => TRUE,
  );

  $form['webform_stripe_payment_private_key'] = array(
    '#title' => t('Private key'),
    '#type' => 'textfield',
    '#default_value' => variable_get('webform_stripe_payment_private_key'),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}
